import 'package:flutter/material.dart';

class FeatureScreen2 extends StatelessWidget {
  const FeatureScreen2({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final List<String> entries = <String>[
      'LINEAR ALGEBRA',
      'CALCULUS',
      'INTRODUCTION TO DATABASE',
      'COMPUTER NETWORKS',
      'ARTIFICIAL INTELLIGENCE',
      'OPRATING SYSTEM'
    ];
    final List<int> colorCodes = <int>[600, 500, 400, 300, 200, 100];
    return Scaffold(
      appBar: AppBar(
        title: const Text("SUBJECTS"),
        centerTitle: true,
        backgroundColor: const Color(0xffFFCB00),
      ),
      body: ListView.separated(
        padding: const EdgeInsets.all(8),
        itemCount: entries.length,
        itemBuilder: (BuildContext context, int index) {
          return Container(
            height: 50,
            color: Colors.orange[colorCodes[index]],
            child: Center(
              child: Text(
                '${entries[index]}',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          );
        },
        separatorBuilder: (BuildContext context, int index) => const Divider(),
      ),
    );
  }
}
