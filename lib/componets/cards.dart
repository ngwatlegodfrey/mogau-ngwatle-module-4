import 'package:flutter/material.dart';

class Cards extends StatelessWidget {
  final String text;
  final Color color, textColor;
  const Cards({
    Key? key,
    required this.text,
    required this.color,
    this.textColor = Colors.white,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Card(
        elevation: 0,
        color: color,
        child: SizedBox(
          width: 300,
          height: 100,
          child: Center(
            child: Text(
              text,
              style: TextStyle(
                color: textColor,
                fontSize: 20,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
